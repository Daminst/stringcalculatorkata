package damian.wetulani;

import java.util.List;
import java.util.stream.Collectors;

public class StringCalculator {
    public int add(String numbers) {
        if(numbers.isEmpty()) {
            return 0;
        }
        List<Integer> integersFromInput = InputStringParser.extractNumbers(numbers);
        checkForNegativeNumbers(integersFromInput);

        return integersFromInput.stream().mapToInt(Integer::intValue)
                .filter(n -> n <= 1000)
                .sum();
    }

    private void checkForNegativeNumbers(List<Integer> integersFromInput) {
        List<Integer> negativeNumbers = integersFromInput.stream()
                .filter(num -> num < 0)
                .collect(Collectors.toList());

        if(!negativeNumbers.isEmpty()) {
            throw new RuntimeException("negatives not allowed " + negativeNumbers);
        }
    }
}
