package damian.wetulani;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputStringParser {
    private static final String DELIMETER_DEFAULT = ",";
    private static final String DELIMETER_NEW_LINE = "\n";

    private static final String DELIMETER_PREFIX = "//";
    private static final String DELIMETER_SUFFIX = "\n";

    public static List<Integer> extractNumbers(String input) {
        List<String> delimeters = new ArrayList<>(extractDefinedDelimeters(input));
        delimeters.add(DELIMETER_NEW_LINE);
        if(input.startsWith(DELIMETER_PREFIX)) {
            input = input.substring(input.indexOf(DELIMETER_SUFFIX) + 1);
        }
        return Stream.of(splitByAllDelimeters(input, delimeters))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public static List<String> extractDefinedDelimeters(String input) {
        if(input.startsWith(DELIMETER_PREFIX)) {
            String delimetersStr = input.substring(DELIMETER_PREFIX.length(), input.indexOf(DELIMETER_SUFFIX));
            return Arrays.asList((delimetersStr.substring(1, delimetersStr.length() - 1).split(Pattern.quote("]["))));
        }
        return Collections.singletonList(DELIMETER_DEFAULT);
    }

    private static String[] splitByAllDelimeters(String input, List<String> delimeters) {
        String[] splittedChunksByDelimeter = input.split(Pattern.quote(delimeters.get(0)));
//        System.out.println("Splitting by -> " + delimeters.get(0) + " --- = " + Arrays.toString(splittedChunksByDelimeter));
        for(String delimeter : delimeters.subList(1, delimeters.size())) {
//            System.out.println("Splitting by -> " + delimeter + " --- = " + Arrays.toString(splittedChunksByDelimeter));
            splittedChunksByDelimeter = splitByDelimeter(splittedChunksByDelimeter, delimeter);
        }
        return splittedChunksByDelimeter;
    }

    private static String[] splitByDelimeter(String[] inputInChunks, String delimeter) {
//        System.out.println("    Del: " + delimeter + "  value=" + Arrays.toString(input));
        List<String> result = new ArrayList<>();
        for(String chunk : inputInChunks) {
            result.addAll(Arrays.asList(chunk.split(Pattern.quote(delimeter))));
        }
//        System.out.println("    return " + result);
        return result.toArray(new String[0]);
    }
}
