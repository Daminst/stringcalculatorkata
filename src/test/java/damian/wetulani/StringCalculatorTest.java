package damian.wetulani;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * String Calculator
 * 1.	Create a simple String calculator with a method int Add(string numbers)
 ****** 1.	The method can take 0, 1 or 2 numbers, and will return their sum (for an empty string it will return 0) for example “” or “1” or “1,2”
 ****** 2.	Start with the simplest test case of an empty string and move to 1 and two numbers
 ****** 3.	Remember to solve things as simply as possible so that you force yourself to write tests you did not think about
 ****** 4.	Remember to refactor after each passing test
 * 2.	Allow the Add method to handle an unknown amount of numbers
 * 3.	Allow the Add method to handle new lines between numbers (instead of commas).
 ****** 1.	the following input is ok:  “1\n2,3”  (will equal 6)
 ****** 2.	the following input is NOT ok:  “1,\n” (not need to prove it - just clarifying)
 * 4.	Support different delimiters
 ****** 1.	to change a delimiter, the beginning of the string will contain a separate line that looks like this:   “//[delimiter]\n[numbers…]” for example “//;\n1;2” should return three where the default delimiter is ‘;’ .
 ****** 2.	the first line is optional. all existing scenarios should still be supported
 * 5.	Calling Add with a negative number will throw an exception “negatives not allowed” - and the negative that was passed.if there are multiple negatives, show all of them in the exception message
 * 6.	Numbers bigger than 1000 should be ignored, so adding 2 + 1001  = 2
 * 7.	Delimiters can be of any length with the following format:  “//[delimiter]\n” for example: “//[***]\n1***2***3” should return 6
 * 8.	Allow multiple delimiters like this:  “//[delim1][delim2]\n” for example “//[*][%]\n1*2%3” should return 6.
 * 9.	make sure you can also handle multiple delimiters with length longer than one char
 **/


class StringCalculatorTest {
    StringCalculator strCalc = new StringCalculator();

    @ParameterizedTest
    @CsvSource(delimiterString = "-->", value = {
        "-->0",//no input equals 0
        "0-->0",
        "1-->1",
        "41-->41",
        "1,2-->3",
        "1,41-->42",
        "1,41,10-->52", //more than 2 numbers allowed
        "1,41,10,10,10,1-->73",
        "1n41n10n10n10n1-->73", //new line allowed
        "1n41,10n10,10n1-->73",
        "//[*]n1*41*10*10*10*1-->73", //custom delimeter at beginning
        "//[*]n41n10*10n10*1-->72",
        "//[#]n41#-1-->exception", //negative number forbidden
        "//[#]n41#-5#1#-10#-19#-100-->exception",
        "//[$]n41$0$0-->41",
        "10,100,1000-->1110", //over 1000 ignored
        "20,200,2000-->220",
        "//[**#**]n20**#**20**#**5-->45", //delimeters of any size
        "//[X][Y][Z]n1X2Y3Z4Z-->10", //multiple delimeters
        "//[**#**][X][oak]n1X2oak3X4**#**-->10", //multiple delimeters of any size
        "//[**#**][X][oak]n1X2oak3X-4**#**-->exception",
    })
    void calculatorTesting(String input, String expectedResult) {
        input = input == null ? "" : input;
        input = input.replaceAll("n", "\n");
        if(expectedResult.equals("exception")) {
            validateExceptionMessage(input);
        } else {
            assertEquals(Integer.parseInt(expectedResult), strCalc.add(input));
        }
    }

    void validateExceptionMessage(String input) {
        try {
            strCalc.add(input);
            fail("Exception not thrown");
        } catch(Exception e) {
            assertEquals("negatives not allowed " + InputStringParser.extractNumbers(input).stream().filter(num -> num < 0).collect(Collectors.toList()), e.getMessage());
        }
    }
}
